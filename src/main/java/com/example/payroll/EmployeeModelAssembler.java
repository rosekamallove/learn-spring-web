package com.example.payroll;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
class EmployeeModelAssembler implements RepresentationModelAssembler<Employee, EntityModel<Employee>> {
  /*
   * In the code earlier, did you notice the repetition in single employee link
   * creation? The code to provide a single link to an employee, as well as to
   * create an "employees" link to the aggregate root, was shown twice. If that
   * raised your concern, good! There’s a solution.
   * 
   * Simply put, you need to define a function that converts Employee objects to
   * EntityModel<Employee> objects. While you could easily code this method
   * yourself, there are benefits down the road of implementing Spring HATEOAS’s
   * RepresentationModelAssembler interface—which will do the work for you.
   * 
   */
  @Override
  public EntityModel<Employee> toModel(Employee employee) {
    return EntityModel.of(employee,
        linkTo(methodOn(EmployeeController.class).one(employee.getId())).withSelfRel(),
        linkTo(methodOn(EmployeeController.class).all()).withRel("employees"));
  }
  /*
   * This simple interface has one method: toModel(). It is based on converting a
   * non-model object (Employee) into a model-based object
   * (EntityModel<Employee>).
   * 
   * All the code you saw earlier in the controller can be moved into this class.
   * And by applying Spring Framework’s @Component annotation, the assembler will
   * be automatically created when the app starts.
   * 
   */
}
