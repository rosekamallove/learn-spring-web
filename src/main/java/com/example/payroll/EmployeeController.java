package com.example.payroll;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
/*
 * @RestController indicates that the data returned by each method will be
 * written straight into the response body instead of rendering a template.
 */
public class EmployeeController {
  private final EmployeeRepository repository;
  private final EmployeeModelAssembler assembler;

  EmployeeController(EmployeeRepository repository, EmployeeModelAssembler assembler) {
    this.repository = repository;
    this.assembler = assembler;
  }

  // Aggregate root
  // tag::get-aggregate-root[]
  @GetMapping("/employees")
  /*
   * CollectionModel<> is another Spring HATEOAS container; it’s aimed at
   * encapsulating collections of resources—instead of a single resource entity,
   * like EntityModel<> from earlier. CollectionModel<>, too, lets you include
   * links.
   * 
   * Don’t let that first statement slip by. What does "encapsulating collections"
   * mean? Collections of employees?
   * Not quite.
   * Since we’re talking REST, it should encapsulate collections of employee
   * resources.
   * 
   * That’s why you fetch all the employees, but then transform them into a list
   * of EntityModel<Employee> objects. (Thanks Java 8 Streams!)
   */
  CollectionModel<EntityModel<Employee>> all() {
    List<EntityModel<Employee>> employees = repository.findAll().stream()
        .map(employee -> EntityModel.of(employee,
            linkTo(methodOn(EmployeeController.class).one(employee.getId())).withSelfRel(),
            linkTo(methodOn(EmployeeController.class).all()).withRel("employees")))
        .collect(Collectors.toList());

    return CollectionModel.of(employees, linkTo(methodOn(EmployeeController.class).all()).withSelfRel());

    /*
     * What is the point of adding all these links? It makes it possible to evolve
     * REST services over time. Existing links can be maintained while new links can
     * be added in the future. Newer clients may take advantage of the new links,
     * while legacy clients can sustain themselves on the old links. This is
     * especially helpful if services get relocated and moved around. As long as the
     * link structure is maintained, clients can STILL find and interact with
     * things.
     * 
     */
  }
  // end::get-aggregate-root[]

  @PostMapping("/employees")
  Employee newEmployee(@RequestBody Employee newEmployee) {
    return repository.save(newEmployee);
  }

  // Single item
  @GetMapping("/employees/{id}")
  /*
   * The return type of the method has changed from Employee to
   * EntityModel<Employee>. EntityModel<T> is a generic container from Spring
   * HATEOAS that includes not only the data but a collection of links.
   */
  EntityModel<Employee> one(@PathVariable Long id) {
    /*
     * Spring HATEOAS will give us the constructs to define a RESTful service and
     * then render it in an acceptable format for client consumption.
     * A critical ingredient to any RESTful service is adding links to relevant
     * operations. To make your controller more RESTful, add links like this:
     */
    Employee employee = repository.findById(id) //
        .orElseThrow(() -> new EmployeeNotFoundException(id));

    /* The follow can be converted now using the assembler */

    // return EntityModel.of(employee, //
    // linkTo(methodOn(EmployeeController.class).one(id)).withSelfRel(),
    // linkTo(methodOn(EmployeeController.class).all()).withRel("employees"));

    return assembler.toModel(employee);

    /*
     * linkTo(methodOn(EmployeeController.class).one(id)).withSelfRel() asks that
     * Spring HATEOAS build a link to the EmployeeController 's one() method, and
     * flag it as a self link.
     * 
     * linkTo(methodOn(EmployeeController.class).all()).withRel("employees") asks
     * Spring HATEOAS to build a link to the aggregate root, all(), and call it
     * "employees".
     */
    /*
     * What do we mean by "build a link"? One of Spring HATEOAS’s core types is
     * Link. It includes a URI and a rel (relation). Links are what empower the web.
     * Before the World Wide Web, other document systems would render information or
     * links, but it was the linking of documents WITH this kind of relationship
     * metadata that stitched the web together.
     * 
     * Roy Fielding encourages building APIs with the same techniques that made the
     * web successful, and links are one of them.
     * 
     */
  }

  @PutMapping("/employees/{id}")
  Employee replaceEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) {
    return repository.findById(id)
        .map(employee -> {
          employee.setName(newEmployee.getName());
          employee.setRole(newEmployee.getRole());
          return repository.save(employee);
        }).orElseGet(() -> {
          newEmployee.setId(id);
          return repository.save(newEmployee);
        });
  }

  @DeleteMapping("/employees/{id}")
  void deleteEmployee(@PathVariable Long id) {
    repository.deleteById(id);
  }
}
